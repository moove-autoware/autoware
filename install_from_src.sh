#!/bin/bash
set -e
mkdir -p moove/autoware
cd moove/autoware/
git clone git@gitlab.com:moove-autoware/autoware.git .
sudo apt update
sudo apt install -y python-catkin-pkg python-rosdep ros-melodic-catkin
sudo apt install -y python3-pip python3-colcon-common-extensions python3-setuptools python3-vcstool
pip3 install -U setuptools

# Install EIGEN
wget https://gitlab.com/libeigen/eigen/-/archive/3.3.7/eigen-3.3.7.tar.gz #Download Eigen
mkdir eigen && tar --strip-components=1 -xzvf eigen-3.3.7.tar.gz -C eigen #Decompress
cd eigen && mkdir build && cd build && cmake .. && make && sudo make install #Build and install
cd ../../ && rm -rf 3.3.7.tar.gz && rm -rf eigen #Remove downloaded and temporary files

vcs import ros/src < autoware.repos
vcs import ros/src < moove.autoware.repos
cd ros
rosdep update
rosdep install -y --from-paths src --ignore-src --rosdistro melodic
