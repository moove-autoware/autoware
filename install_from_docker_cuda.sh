#!/bin/bash

mkdir -p moove/autoware
cd moove/autoware/
git clone git@gitlab.com:moove-autoware/autoware.git .
sudo apt install -y python3-vcstool

vcs import ros/src < autoware.repos
vcs import ros/src < moove.autoware.repos

cd docker
./build_cuda.sh
