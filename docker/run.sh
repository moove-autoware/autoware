#!/bin/bash

set -e

# Convert a relative directory path to absolute
function abspath() {
    local path=$1
    if [ ! -d $path ]; then
	exit 1
    fi
    pushd $path > /dev/null
    echo $(pwd)
    popd > /dev/null
}


# Default settings
MOOVE_HOST_DIR="$( dirname "${BASH_SOURCE[0]}" )/.."
MOOVE_HOST_DIR=$(abspath "$MOOVE_HOST_DIR")
USER_ID="$(id -u)"

XSOCK=/tmp/.X11-unix
XAUTH=$HOME/.Xauthority

SHARED_DOCKER_DIR=/home/moove/shared_dir
SHARED_HOST_DIR=$HOME/shared_dir

MOOVE_DOCKER_DIR=/home/moove/autoware

VOLUMES="--volume=$XSOCK:$XSOCK:rw
         --volume=$XAUTH:$XAUTH:rw
         --volume=$SHARED_HOST_DIR:$SHARED_DOCKER_DIR:rw
         --volume=$MOOVE_HOST_DIR:$MOOVE_DOCKER_DIR"

# Create the shared directory in advance to ensure it is owned by the host user
mkdir -p $SHARED_HOST_DIR

docker run \
    -it --rm \
    $VOLUMES \
    --env="XAUTHORITY=${XAUTH}" \
    --env="DISPLAY=${DISPLAY}" \
    --env="USER_ID=$USER_ID" \
    --privileged \
    --net=host \
    moove-autoware:latest
