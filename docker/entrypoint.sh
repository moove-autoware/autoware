#!/bin/bash

set -e

cd /home/moove
echo "source ~/autoware/ros/install/setup.bash" >> .bashrc

# If no command is provided, set bash to start interactive shell
if [ -z "$1" ]; then
    set - "/bin/bash" -l
fi

# Run the provided command using user 'autoware'
exec "$@"
