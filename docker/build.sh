#!/bin/bash

set -e

USER_ID="$(id -u)"
GROUP_ID="$(id -g)"

docker build \
    --rm \
    --tag moove-autoware \
    --build-arg USER_ID=$USER_ID \
    --build-arg GROUP_ID=$GROUP_ID \
    --file Dockerfile.moove .
